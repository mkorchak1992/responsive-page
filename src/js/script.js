"use strict";
const payBtn = document.querySelectorAll('.companies__section-block_button-container-payButton');
const url = 'https://api.ipify.org/?format=json';
let result = [];

payBtn.forEach((btn) => {
  btn.addEventListener('click', async (event) => {
    const myResponse = await fetch(url);
    const addressIP = await myResponse.json();

    result.push({IP: addressIP.ip},
      {ID: btn.id},
      {time: (new Date().toLocaleString())});
    console.log(result);
  });

})

